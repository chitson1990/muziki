package muzikinet.muziki.softit.muziki.data;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import muzikinet.muziki.softit.muziki.activities.NavigationDrawerActivity;
import muzikinet.muziki.softit.muziki.tools.HardCodedValues;
import android.net.Uri;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import tonyostudios.ambience.AmbientTrack;

/**
 * Created by manyanda on 15/08/15.
 */
public class SongDBHelper extends SQLiteOpenHelper {

    private static final String KEY_ID = "id";
    private static final String KEY_TITLE = "title";
    private static final String KEY_ARTIST = "artist";
    private static final String KEY_URL = "url";

    private static final String DATABASE_NAME = "muzikiDB";
    private static final int DATABASE_VERSION = 2;
    private static final String TABLE_NAME = "songs";
    private static final String fetchQuery = "SELECT  * FROM " + TABLE_NAME;

    private static final String TABLE_CREATE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " (" +
                     KEY_ID + " INTEGER PRIMARY KEY, " +
                     KEY_ARTIST + " TEXT, " +
                     KEY_TITLE + " TEXT,"  +
                     KEY_URL + " TEXT" +
                    ");";


    private static final String[] COLUMNS = {KEY_ID,KEY_TITLE,KEY_ARTIST,KEY_URL};

    private Context context;
    public SongDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TABLE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF TABLE EXISTS " + TABLE_NAME);

        this.onCreate(db);
    }

    public void addSong(AmbientTrack track) {

        if(!canSave(track)) {
            return;
        }

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_ID ,track.getId());
        values.put(KEY_TITLE , track.getName());
        values.put(KEY_ARTIST, track.getArtistName());
        values.put(KEY_URL , track.getAudioUri().toString());

        db.insert(TABLE_NAME, // table
                null, //nullColumnHack
                values); // key/value -> keys = column names/ values = column values

        // 4. close
        db.close();
        
        Toast.makeText(this.context,"Song saved.",Toast.LENGTH_SHORT).show();
    }

    public boolean canSave(AmbientTrack track) {
        if(checkIfTrackAlreadySaved(track)) {
            Toast.makeText(this.context,"The song " + track.getName() + " is already saved.",
                    Toast.LENGTH_SHORT).show();
            return false;
        }

        if(!maxAllowedSongsNotReached()) {
            Toast.makeText(this.context,"You can save a max of "+ HardCodedValues.MAX_SAVED_TRACKS
                    +" songs", Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    public boolean maxAllowedSongsNotReached() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(fetchQuery, null);
        int cnt = cursor.getCount();
        cursor.close();
        return cnt < HardCodedValues.MAX_SAVED_TRACKS;
    }

    public boolean checkIfTrackAlreadySaved(AmbientTrack track) {
        SQLiteDatabase db = this.getReadableDatabase();

        // 2. build query
        Cursor cursor =
                db.query(TABLE_NAME, // a. table
                        COLUMNS, // b. column names
                        " id = ?", // c. selections
                        new String[]{String.valueOf(track.getId())}, // d. selections args
                        null, // e. group by
                        null, // f. having
                        null, // g. order by
                        null); // h. limit

        // 3. if we got results get the first one
        return cursor.getCount() > 0;

    }

   public AmbientTrack getSong(int id) {
       SQLiteDatabase db = this.getReadableDatabase();

       // 2. build query
       Cursor cursor =
               db.query(TABLE_NAME, // a. table
                       COLUMNS, // b. column names
                       " id = ?", // c. selections
                       new String[] { String.valueOf(id) }, // d. selections args
                       null, // e. group by
                       null, // f. having
                       null, // g. order by
                       null); // h. limit

       // 3. if we got results get the first one
       if (cursor != null)
           cursor.moveToFirst();

       return createTrack(cursor);
   }

    private AmbientTrack createTrack(Cursor cursor) {
        AmbientTrack track = AmbientTrack.newInstance();
        track.setId(Integer.parseInt(cursor.getString(0)));
        track.setArtistName(cursor.getString(1));
        track.setName(cursor.getString(2));
        track.setAudioUri(Uri.parse(getDirPath() + cursor.getString(3)));
        return track;
    }

    private String getDirPath() {
        return this.context.getFilesDir().toString() + "/";
    }

    public List<AmbientTrack> getAllSongs() {
        List<AmbientTrack> tracks = new ArrayList<>();

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(fetchQuery, null);

        if (cursor.moveToFirst()) {
            do {
                tracks.add(createTrack(cursor));
            } while (cursor.moveToNext());
        }

        return tracks;
    }

    private void deleteSong(AmbientTrack track) {

        SQLiteDatabase db = this.getWritableDatabase();

        db.delete(TABLE_NAME,
                KEY_ID + " = ?",
                new String[]{String.valueOf(track.getId())});
        db.close();

        String url = track.getAudioUri().toString();
        String[] urlContents = url.split("/");
        File file = new File( urlContents[urlContents.length -1]);
        file.delete();

        Toast.makeText(this.context, "Deletion succesful.", Toast.LENGTH_SHORT);
    }

    public int updateSong(AmbientTrack track) {

        // 1. get reference to writable DB
        SQLiteDatabase db = this.getWritableDatabase();

        // 2. create ContentValues to add key "column"/value
        ContentValues values = new ContentValues();
        values.put(KEY_TITLE, track.getName());
        values.put(KEY_ARTIST, track.getArtistName());
        values.put(KEY_URL, track.getAudioUri().toString());

        // 3. updating row
        int i = db.update(TABLE_NAME, //table
                values, // column value
                KEY_ID + " = ?", // selections
                new String[]{String.valueOf(track.getId())}); //selection args

        // 4. close
        db.close();

        return i;

    }

    public void confirmDeletionOf(final AmbientTrack track) {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        deleteSong(track);
                        NavigationDrawerActivity.narrowToDrawer(context).deleteTrack(track);
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("Are you sure?").setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();
    }
}
