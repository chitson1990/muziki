package muzikinet.muziki.softit.muziki.data;

/**
 * Created by manyanda on 03/06/15.
 */

import android.content.Context;
import android.muziki.softit.muziki.R;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Map;

import muzikinet.muziki.softit.muziki.activities.NavigationDrawerActivity;
import muzikinet.muziki.softit.muziki.tools.HardCodedValues;
import muzikinet.muziki.softit.muziki.tools.REQUEST;
import muzikinet.muziki.softit.muziki.tools.Tasks.DownloadWebpageTask;
import tonyostudios.ambience.AmbientTrack;

public class SongManager {
    private SongDBHelper db;
    private boolean isConnected  = false;

    REQUEST req=REQUEST.NEW;
    private Context context;

    private String url;
    private Map<String, String> queryData;

    public SongManager(REQUEST req, Context mActivity) {
        this.req =req;
        this.context=mActivity;
        switch (req) {
            case SEARCH:
                url= HardCodedValues.searchSongUrl;
                break;
            case POPULAR:
                url= HardCodedValues.popularSongsUrl;
                break;
            case NEW:
                url=HardCodedValues.newSongsUrl;
                break;
            case SAVED:
                db = new SongDBHelper(this.context);
                break;
            default:
                break;
        }
    }

    /**
     * Function to read all mp3 files from sdcard
     * and store the details in ArrayList
     * */
    private void renderFromDB(){
        displayTracks(new ArrayList<>(db.getAllSongs()));
    }

    public void render(Map<String, String> queryData) {
        this.queryData= queryData;
        switch (req) {
            case SAVED:
                renderFromDB();
                break;
            default:
                renderFromServer();
        }
    }

    public Map<String, String> getQueryData() {
        return queryData;
    }

    /**
     * Function to read all mp3 audios from
     * Server by passing the url and application context..
     */

    private void renderFromServer() {
        ConnectivityManager cm =
                (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();

        if(!isConnected) {
            Toast.makeText(context, R.string.noConnection,Toast.LENGTH_LONG);
            return; // null;
        }


        NavigationDrawerActivity.dialog.show();

        /* executeOnExecutor() executes task in a different thread in case there's another AsyncTask
          running =>valid in post honeycomb, before honeycomb it runs sequentially as execute() ie. waits for
           the executing task to finish.*/
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
            new DownloadWebpageTask(this).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, url);
        else
            new DownloadWebpageTask(this).execute(url);

    }

    public void displayTracks(ArrayList<AmbientTrack> tracks) {
        NavigationDrawerActivity.narrowToDrawer(context).displayTracks(tracks);
    }

}
