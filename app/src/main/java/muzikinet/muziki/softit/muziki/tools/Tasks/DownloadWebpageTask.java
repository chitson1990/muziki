package muzikinet.muziki.softit.muziki.tools.Tasks;

import muzikinet.muziki.softit.muziki.activities.NavigationDrawerActivity;
import muzikinet.muziki.softit.muziki.data.SongManager;
import muzikinet.muziki.softit.muziki.tools.HardCodedValues;
import android.net.Uri;
import android.os.AsyncTask;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import tonyostudios.ambience.AmbientTrack;

/**
 * Created by manyanda on 18/08/15.
 */
public class DownloadWebpageTask extends AsyncTask<String, Void, Document> {

   private SongManager manager;

    public DownloadWebpageTask(SongManager manager) {
        this.manager = manager;
    }

    @Override
    protected Document doInBackground(String... urls) {

        // params comes from the execute() call: params[0] is the url.
        try {
            return downloadUrl(urls[0],manager.getQueryData());
        } catch (IOException e) {
            return null;
        }
    }
    // onPostExecute displays the results of the AsyncTask.
    @Override
    protected void onPostExecute(Document result) {
        NavigationDrawerActivity.dialog.cancel();
        if(result==null) {
            return;
        }

        manager.displayTracks(extractTracksFromDocument(result));
    }

    /**
     * Given a URL, create a DOM from it. Analyse the DOM to get the kind of results received.
     * */
    private Document downloadUrl(String myurl, Map<String,String> queryData) throws IOException {
        return Jsoup.connect(myurl).data(queryData).timeout(5000).post();
    }

    private ArrayList<AmbientTrack> extractTracksFromDocument(Document doc) {
        ArrayList<AmbientTrack> tracks = new ArrayList<>();
        Elements audios = doc.select(".song");
        AmbientTrack track;
        for (Element song : audios) {
            track = AmbientTrack.newInstance();
            Elements children= song.getAllElements();
            for(Element child:children) {
                if(child.hasClass("image")) {
                    track.setAlbumImageUri(Uri.parse(HardCodedValues.BASE_URL + child.text()));
                } else if (child.hasClass("url")) {
                    track.setAudioUri(Uri.parse(HardCodedValues.BASE_URL+child.text().split(".mp3")[0] + ".mp3"));
                } else if (child.hasClass("id")) {
                    track.setId(Long.parseLong(child.text()));
                } else if (child.hasClass("author")) {
                    track.setArtistName(child.text());
                } else {
                    track.setName(child.text());
                }
            }
            tracks.add(track);

        }
        return tracks;
    }
}
