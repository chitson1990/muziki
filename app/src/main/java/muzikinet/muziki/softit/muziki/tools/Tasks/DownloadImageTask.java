package muzikinet.muziki.softit.muziki.tools.Tasks;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.muziki.softit.muziki.R;
import android.os.AsyncTask;
import android.widget.ImageView;

import java.io.InputStream;

import muzikinet.muziki.softit.muziki.tools.HardCodedValues;

/**
 * Created by manyanda on 08/06/15.
 */
public class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
    ImageView bmImage;

    public DownloadImageTask(ImageView bmImage) {
        this.bmImage = bmImage;
    }

    protected Bitmap doInBackground(String... urls) {
        String urldisplay = urls[0];
        Bitmap image = null;


        if(urldisplay.contains(HardCodedValues.dummyImage) || urldisplay.length() == 0) {
            return image;
        }

        try {
            InputStream in = new java.net.URL(urldisplay).openStream();
            image = BitmapFactory.decodeStream(in);
        } catch (Exception e) {
        }
        return image;
    }

    protected void onPostExecute(Bitmap result) {
        if(result != null) {
            bmImage.setImageBitmap(result);
            return;
        }

        bmImage.setImageResource(R.drawable.dummy);
    }
}
