package muzikinet.muziki.softit.muziki.tools;

/**
 * Created by manyanda on 08/06/15.
 */
public class HardCodedValues {
    /**
     * Social nets
     */
    public static final String gplusPageID="101897504559009282778";
    public static final String gplusBaseURL="https://plus.google.com/";
    public static final String gplusPosts="/posts";
    public static final String fbPageID="fb://page/163604847103318"; // To open app
    public static final String fbPageURL="https://www.facebook.com/Muziki.net?fref=ts";
    public static final String twitterPageID="twitter://user?user_id=488280653";
    public static final String twitterPageURL="https://twitter.com/MuzikiNet";

    /**
     * Front
     */
    public static final String dummyImage ="dummy";
    public static final int MAX_SAVED_TRACKS = 30;
    public static String[] dropDownList= new String[]{"Play", "Share", "Save"};

    /**
     * Server
     */
    public static final String BASE_URL = "http://muziki.net/";
    public static final String actionPath=BASE_URL+"src/service/";
    public static final String popularSongsUrl=actionPath+"getPopularMusicMobile.php";
    public static final String newSongsUrl=actionPath+"getMusicAsyncMobile.php";
    public static final String searchSongUrl=actionPath+"searchSongMobile.php";
    public static final String query="query";

    /**
     * Player's states
     */

    public static final String isPlaying = "isPlaying";
    public static final String isRepeatOne = "repeatOne";
    public static final String isRepeatAll = "repeatAll";
    public static final String isRepeatOff = "repeatOff";
    public static final String isShuffled = "shuffled";
}
