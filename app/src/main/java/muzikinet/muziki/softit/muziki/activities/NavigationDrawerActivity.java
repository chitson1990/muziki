package muzikinet.muziki.softit.muziki.activities;

import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.muziki.softit.muziki.R;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import muzikinet.muziki.softit.muziki.data.SongManager;
import muzikinet.muziki.softit.muziki.tools.HardCodedValues;
import muzikinet.muziki.softit.muziki.tools.REQUEST;
import tonyostudios.ambience.Ambience;
import tonyostudios.ambience.AmbientTrack;

public class NavigationDrawerActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {
    private DrawerLayout drawerLayout;
    protected ListView listView;
    private ListView listViewSocialNets;
    protected String[] navDrawerItems;
    private ActionBarDrawerToggle drawerListener;
    private RelativeLayout drawerRelativeLayout;
    private int[] socialNetItems = {R.drawable.facebook,R.drawable.twitter,R.drawable.gplus};
    private ImageButton btnFacebook,btnTwitter,btnGplus;
    protected static int position;
    protected FrameLayout frameLayout;
    private static boolean isLaunch = true;
    public static ProgressDialog dialog;
    protected ArrayList<AmbientTrack> tracks;
    public static NavigationDrawerActivity mActivity;
    public static REQUEST request;
    public static Map<String,Boolean> playerState;
    protected SongManager manager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppThemeRed);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.navigation_drawer_layout);
        btnFacebook = (ImageButton) findViewById(R.id.facebookImgBtn);
        btnTwitter = (ImageButton) findViewById(R.id.twitterImgBtn);
        btnGplus = (ImageButton) findViewById(R.id.gplusImgBtn);
        frameLayout = (FrameLayout)findViewById(R.id.content_frame);
        drawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);
        listView =(ListView) findViewById(R.id.list_view_drawer);
        drawerRelativeLayout = (RelativeLayout) findViewById(R.id.left_drawer);
        navDrawerItems = getResources().getStringArray(R.array.navigation_drawer_list);
        listView.setAdapter(new ArrayAdapter<>(this,android.R.layout.simple_list_item_1,navDrawerItems));
        listView.setOnItemClickListener(this);
        dialog = new ProgressDialog(this);
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        btnFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFacebookIntent();
            }
        });
        btnTwitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openTwitterIntent();
            }
        });
        btnGplus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openGPlusIntent();
            }
        });
        drawerListener = new ActionBarDrawerToggle(this,drawerLayout,R.string.drawer_open,R.string.drawer_close){
            @Override
            public void onDrawerOpened(View drawerView) {
                setTitle(R.string.app_name);
            }

            @Override
            public void onDrawerClosed(View drawerView) {

                setTitle(navDrawerItems[position]);
            }
        };
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_drawer);
        drawerLayout.setDrawerListener(drawerListener);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        drawerListener.syncState();
        if(isLaunch){
/**
 *Setting this flag false so that next time it will not open our first activity.
 *We have to use this flag because we are using this BaseActivity as parent activity to our other activity.
 *In this case this base activity will always be call when any child activity will launch.
 */
            isLaunch = false;
            /*
             * Popular as home page.
             */
            openActivity(1);
        }

        initialPlayerState();
        Ambience.turnOn(this);
    }

    protected void onDestroy() {
        super.onDestroy();
        Ambience.turnOff();
    }

    private static void initialPlayerState() {
        if(playerState != null )
            return;

        playerState = new HashMap<>(4);
        playerState.put(HardCodedValues.isPlaying, false);
        playerState.put(HardCodedValues.isShuffled,false);
        playerState.put(HardCodedValues.isRepeatOff, true);
        playerState.put(HardCodedValues.isRepeatOne, false);
        playerState.put(HardCodedValues.isRepeatAll, false);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView =
                (SearchView) menu.findItem(R.id.search).getActionView();
       searchView.setSearchableInfo(
               searchManager.getSearchableInfo(getComponentName()));

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        if (drawerListener.onOptionsItemSelected(item)) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        openActivity(position);

    }

    protected void openActivity(int position) {
        drawerLayout.closeDrawer(Gravity.LEFT);
        selectItem(position);
        NavigationDrawerActivity.position = position;
        setRequestFromItemPosition(position);
        setPopDropDownList();
        startActivity();
    }

    private void startActivity() {
        if( request == REQUEST.SAVED) {
            startActivity(new Intent(NavigationDrawerActivity.this, ListActivity.class));
            return;
        }
        startActivity(new Intent(NavigationDrawerActivity.this, GridActivity.class));
    }

    private static void setRequestFromItemPosition(int position) {
        switch(position) {
            case 0:
                request = REQUEST.NEW;
                break;
            case 1:
                request = REQUEST.POPULAR;
                break;
            case 2:
                request = REQUEST.SAVED;
                break;
            default:
                request = REQUEST.SEARCH;
        }
    }

    private static void setPopDropDownList() {
        HardCodedValues.dropDownList[2]= "Save";
        if(request == REQUEST.SAVED) {
            HardCodedValues.dropDownList[2]= "Remove";
        }
    }

    public void selectItem(int position) {
        listView.setItemChecked(position, true);
         setTitle(navDrawerItems[position]);
    }
    /* We can override onBackPressed method to toggle navigation drawer*/
    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(Gravity.LEFT)) drawerLayout.closeDrawer(Gravity.LEFT);
        return;

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerListener.onConfigurationChanged(newConfig);
    }

    public void openFacebookIntent() {
        Intent fbIntent;
        try {
            getPackageManager().getPackageInfo("com.facebook.katana", 0);
            fbIntent =new Intent(Intent.ACTION_VIEW, Uri.parse(HardCodedValues.fbPageID));
            fbIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        } catch (Exception e) {
            fbIntent =new Intent(Intent.ACTION_VIEW, Uri.parse(HardCodedValues.fbPageURL));
        }
        startActivity(fbIntent);
    }

    public void openGPlusIntent() {
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setClassName("com.google.android.apps.plus",
                    "com.google.android.apps.plus.phone.UrlGatewayActivity");
            intent.putExtra("customAppUri", HardCodedValues.gplusPageID);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse(HardCodedValues.gplusBaseURL + HardCodedValues.gplusPageID + HardCodedValues.gplusPosts)));
        }
    }

    public void openTwitterIntent() {
        try {
            getPackageManager().getPackageInfo("com.twitter.android", 0);
            Intent intent = new Intent(Intent.ACTION_VIEW,
                    Uri.parse(HardCodedValues.twitterPageID));
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);        
            startActivity(intent);

        }catch (Exception e) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse(HardCodedValues.twitterPageURL)));
        }
    }

    public void shareIntent(AmbientTrack track) {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        String textToShare = "Listen to "+ track.getName() + " by " +
                track.getArtistName() + " using muziki's mobile app or " +
                "go to http://muziki.net/#about"+track.getId();
        sendIntent.putExtra(Intent.EXTRA_TEXT, textToShare);
        sendIntent.setType("text/plain");
        startActivity(Intent.createChooser(sendIntent, "Share using"));
    }

    public void shareIntent(int position) {
       shareIntent(getTrack(position));
    }


    public AmbientTrack getTrack(int position) {
        return tracks.get(position);
    }

    public void displayTracks(ArrayList<AmbientTrack> tracks) {
        this.tracks = new ArrayList(tracks);
        Ambience.activeInstance().setPlaylistTo(this.tracks);
    }

    public static NavigationDrawerActivity narrowToDrawer(Context context) {
        return (NavigationDrawerActivity)context;
    }

    public void deleteTrack(AmbientTrack track) {
        tracks.remove(track);
        displayTracks(tracks);
        Ambience.activeInstance().setPlaylistTo(tracks);
    }
    private static void toggleWhenRepeatClicked(String value) {
        if(value.equals(HardCodedValues.isRepeatOne)) {
            playerState.put(HardCodedValues.isRepeatOne,true);
            playerState.put(HardCodedValues.isRepeatAll,false);
            playerState.put(HardCodedValues.isRepeatOff,false);
        } else if (value.equals(HardCodedValues.isRepeatAll)) {
            playerState.put(HardCodedValues.isRepeatAll,true);
            playerState.put(HardCodedValues.isRepeatOne,false);
            playerState.put(HardCodedValues.isRepeatOff,false);
        } else {
            playerState.put(HardCodedValues.isRepeatOff,true);
            playerState.put(HardCodedValues.isRepeatOne,false);
            playerState.put(HardCodedValues.isRepeatAll,false);
        }
    }
    /**
     * A music fragment containing a simple view.
     */

    public static class MusicFragment extends Fragment implements Ambience.AmbientListener {

        private SeekBar mSeekBar;
        private TextView mPlaybackTotalTime;
        private TextView mPlaybackCurrentTime;
        private View mPlayButton;
        private View mPauseButton;
        private View mFowardButton;
        private View mPreviousButton;
        private TextView mNowPlayingTrack;
        private TextView mNowPlayingArtistName;

        private ImageView repeatView;
        private ImageView shuffleView;

        public MusicFragment() {

        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
        }

        @Override
        public void onResume() {
            super.onResume();

            //start getting updates for the ambient service
            Ambience.activeInstance().listenForUpdatesWith(this);
        }

        @Override
        public void onPause() {
            super.onPause();

            //Stop getting updates for the ambient service
            Ambience.activeInstance()
                    .stopListeningForUpdates();
        }


        public String getFormattedTimeString(int position)
        {
            int totalDuration = position;


            // set total time as the audio is being played
            String formattedString = String.format(
                    "%02d:%02d",
                    TimeUnit.MILLISECONDS.toMinutes((long) totalDuration),
                    TimeUnit.MILLISECONDS.toSeconds((long) totalDuration)
                            - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS
                            .toMinutes((long) totalDuration)));


            return formattedString;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {

            View rootView = inflater.inflate(R.layout.fragment_main, container, false);

            //set adapter to musicList ListView

            setupPlayerView(rootView);


            return rootView;
        }

        private void setupPlayerView(View view) {

            mSeekBar = (SeekBar) view.findViewById(R.id.media_seekbar);
            mPauseButton = view.findViewById(R.id.pause);
            mPlayButton = view.findViewById(R.id.play);
            mPlaybackCurrentTime = (TextView) view.findViewById(R.id.playback_time);
            mPlaybackTotalTime = (TextView) view.findViewById(R.id.duration_time);
            mFowardButton = view.findViewById(R.id.forward);
            mPreviousButton = view.findViewById(R.id.rewind);
            mNowPlayingTrack = (TextView) view.findViewById(R.id.display_track_name);
            mNowPlayingArtistName = (TextView) view.findViewById(R.id.display_track_album);

            mSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int i, boolean b) {


                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                    Ambience.activeInstance().seekTo(seekBar.getProgress());
                }
            });

            mPauseButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    playerState.put(HardCodedValues.isPlaying,false);
                    Ambience.activeInstance().pause();
                }
            });


            mPreviousButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Ambience.activeInstance().previous();
                }
            });

            mFowardButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Ambience.activeInstance().skip();
                }
            });


            repeatView = (ImageView) view.findViewById(R.id.repeat);

            repeatView.setOnClickListener(initialRepeatListener());


            shuffleView = (ImageView) view.findViewById(R.id.shuffle);

            shuffleView.setOnClickListener(initialShuffleListener()) ;

            disablePlayerButton();

            final ImageView volumeImg = (ImageView) view.findViewById(R.id.volume_image);

            SeekBar volume = (SeekBar) view.findViewById(R.id.volume);

            volume.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    float value = ((float) progress / (float) 10);
                    //send volume request
                    if(progress <= 0.5) {
                        volumeImg.setImageResource(R.drawable.mute);
                    }
                    else if (progress > 0.5 && progress <= 3 ) {
                        volumeImg.setImageResource(R.drawable.audio_active_30);
                    } else if ( progress > 3 && progress <=6 ) {
                        volumeImg.setImageResource(R.drawable.audio_active_60);
                    } else {
                        volumeImg.setImageResource(R.drawable.audio_active_100);
                    }

                    Ambience.activeInstance().setVolumeTo(value);

                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                }
            });

        }


        public View.OnClickListener shuffleOn() {
            return new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Ambience.activeInstance().shufflePlaylist();

                    Toast.makeText(mActivity, "Shuffle On", Toast.LENGTH_SHORT).show();
                    shuffleView.setOnClickListener(shuffleOff());
                    playerState.put(HardCodedValues.isShuffled,true);

                }
            };
        }

        public View.OnClickListener shuffleOff() {
            return new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //send unshuffle request
                    Ambience.activeInstance().unShufflePlaylist();

                    Toast.makeText(mActivity, "Shuffle Off", Toast.LENGTH_SHORT).show();
                    shuffleView.setOnClickListener(shuffleOn());
                    playerState.put(HardCodedValues.isShuffled, false);
                }
            };

        }

        private View.OnClickListener initialShuffleListener() {
            if(playerState.get(HardCodedValues.isShuffled)) {
                return shuffleOn();
            }

            return shuffleOff();
        }

        public View.OnClickListener repeatOff() {
            return  new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //send turnRepeatOff request
                    Ambience.activeInstance().turnRepeatOff();
                    Toast.makeText(mActivity, "Repeat Off", Toast.LENGTH_SHORT).show();
                    repeatView.setImageResource(R.drawable.repeat_off);
                    repeatView.setOnClickListener(repeatOne());
                    toggleWhenRepeatClicked(HardCodedValues.isRepeatOff);
                }
            };
        }

        public View.OnClickListener repeatOne() {
            return new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Ambience.activeInstance().repeatASingleTrack();

                    Toast.makeText(mActivity, "Repeat One", Toast.LENGTH_SHORT).show();
                    repeatView.setImageResource(R.drawable.repeat2icon);
                    repeatView.setOnClickListener(repeatAll());
                    toggleWhenRepeatClicked(HardCodedValues.isRepeatOne);
                }
            };
        }

        public View.OnClickListener repeatAll() {
            return new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Ambience.activeInstance().repeatAllTracks();
                    Toast.makeText(mActivity, "Repeat All", Toast.LENGTH_SHORT).show();
                    repeatView.setImageResource(R.drawable.repeat_all);
                    repeatView.setOnClickListener(repeatOff());
                    toggleWhenRepeatClicked(HardCodedValues.isRepeatAll);
                }
            };
        }

        public View.OnClickListener initialRepeatListener() {
            if(playerState.get(HardCodedValues.isRepeatOne)) {
                return repeatAll();
            } else if (playerState.get(HardCodedValues.isRepeatAll)) {
                return repeatOff();
            }
            return repeatOne();
        }

        public void setPauseView() {

            if (mPlayButton != null) {
                mPlayButton.setVisibility(View.VISIBLE);
            }

            if (mPauseButton != null) {
                mPauseButton.setVisibility(View.GONE);
            }
        }

        public void setPlayView()
        {
            if (mPlayButton != null) {
                mPlayButton.setVisibility(View.GONE);
            }

            if (mPauseButton != null) {
                mPauseButton.setVisibility(View.VISIBLE);
            }
        }



        public void hideNowPlayingTextViews()
        {
            if(mNowPlayingTrack != null)
            {
                mNowPlayingTrack.setVisibility(View.GONE);
            }

            if(mNowPlayingArtistName != null)
            {
                mNowPlayingArtistName.setVisibility(View.GONE);
            }
        }


        private void disablePlayerButton()
        {
            if(playerState.get(HardCodedValues.isPlaying)) {
                setPlayView();
            } else {
                setPauseView();
            }

            if(playerState.get(HardCodedValues.isShuffled)) {
                shuffleView.setImageResource(R.drawable.shuffleicon);
            } else {
                shuffleView.setImageResource(R.drawable.shuffleicon);
            }

            if(playerState.get(HardCodedValues.isRepeatOne)) {
                repeatView.setImageResource(R.drawable.repeat2icon);
            } else if (playerState.get(HardCodedValues.isRepeatAll)) {
                repeatView.setImageResource(R.drawable.repeat_all);
            } else {
                repeatView.setImageResource(R.drawable.repeat_off);
            }
        }

        private void enablePlayerButtons()
        {
            mSeekBar.setEnabled(true);
            mPreviousButton.setEnabled(true);
            mFowardButton.setEnabled(true);
            mPauseButton.setEnabled(true);
            mPlayButton.setEnabled(true);
        }

        public void showNowPlayingTextViews()
        {

            if(mNowPlayingTrack != null)
            {
                mNowPlayingTrack.setVisibility(View.VISIBLE);
            }

            if(mNowPlayingArtistName != null)
            {
                mNowPlayingArtistName.setVisibility(View.VISIBLE);
            }
        }

        @Override
        public void ambienceIsPreppingTrack() {
            disablePlayerButton();
        }

        @Override
        public void ambienceTrackDuration(int time) {
            if (mPlaybackTotalTime != null) {
                mPlaybackTotalTime.setText(getFormattedTimeString(time));
            }

            if(mSeekBar != null)
            {
                mSeekBar.setMax(time);
            }
        }

        @Override
        public void ambiencePlayingTrack(AmbientTrack track) {
            if(mNowPlayingTrack != null)
            {
                mNowPlayingTrack.setText(track.getName());
            }

            if(mNowPlayingArtistName != null)
            {
                mNowPlayingArtistName.setText(track.getArtistName());

            }

            showNowPlayingTextViews();
            playerState.put(HardCodedValues.isPlaying,true);
        }

        @Override
        public void ambienceTrackCurrentProgress(int time) {
            if (mPlaybackTotalTime != null) {

                mPlaybackCurrentTime.setText(getFormattedTimeString(time));
            }

            if(mSeekBar != null)
            {
                mSeekBar.setProgress(time);
            }
        }

        @Override
        public void ambienceTrackIsPlaying() {
            enablePlayerButtons();
            setPlayView();
        }

        @Override
        public void ambienceTrackIsPaused() {
            setPauseView();
            mPlayButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    playerState.put(HardCodedValues.isPlaying,true);
                    //send resume request
                    Ambience.activeInstance()
                            .resume();

                }
            });
        }

        @Override
        public void ambienceTrackHasStopped() {
            mPlayButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    //send resume request

                    playerState.put(HardCodedValues.isPlaying,true);
                    Ambience.activeInstance()
                            .resume();


                }
            });
        }

        @Override
        public void ambiencePlaylistCompleted() {

            setPauseView();
        }

        @Override
        public void ambienceErrorOccurred() {
            //not implemented
        }

        @Override
        public void ambienceServiceStarted(Ambience activeInstance) {
            //not implemented
        }

        @Override
        public void ambienceServiceStopped(Ambience activeInstance) {
            //not implemented
        }
    }
}
