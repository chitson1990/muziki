package muzikinet.muziki.softit.muziki.activities;

import android.muziki.softit.muziki.R;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.kylewbanks.animlv.AnimatedListView;
import com.kylewbanks.animlv.AnimatedListViewAdapter;
import com.kylewbanks.animlv.AnimatedListViewObjectMapper;

import java.util.ArrayList;
import java.util.HashMap;

import muzikinet.muziki.softit.muziki.data.SongManager;
import muzikinet.muziki.softit.muziki.tools.HardCodedValues;
import muzikinet.muziki.softit.muziki.tools.Tasks.DownloadImageTask;
import muzikinet.muziki.softit.muziki.viewholder.MuzikiPopUpWindow;
import tonyostudios.ambience.Ambience;
import tonyostudios.ambience.AmbientTrack;

/**
 * Created by manyanda on 19/09/15.
 */
public class ListActivity extends NavigationDrawerActivity{

    protected int viewResourceId = R.layout.recycler_item;
    protected   AnimatedListView animatedListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppThemeRed);
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.fragment_main, frameLayout);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container1, new MusicFragment())
                    .commit();
        }

        setTitle("SAVED SONGS");

        mActivity = this;

        animatedListView  = (AnimatedListView)findViewById(R.id.my_list);
        animatedListViewOnItemClickListener();
        animatedListViewOnItemLongClickListener();
        manager = new SongManager(request,this);
        manager.render(new HashMap<String, String>());
    }


    protected   void animatedListViewOnItemClickListener() {
        animatedListView.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Ambience.activeInstance().PlayFromPosition(position);
                    }
                }
        );
    }


    protected   void animatedListViewOnItemLongClickListener() {
        animatedListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

            public boolean onItemLongClick(AdapterView<?> arg0, View view,
                                           int pos, long id) {
                MuzikiPopUpWindow.newPopInstance(mActivity, HardCodedValues.dropDownList, pos).showAsDropDown(view, -5, 0);
                return true;
            }
        });
    }

    @Override
    //Display data in the AnimatedListView
    public void displayTracks(ArrayList<AmbientTrack> postList) {
        if(postList.size() == 0)
            Toast.makeText(ListActivity.this, "Your Saved Songs list is empty\n", Toast.LENGTH_SHORT).show();
        else {
            super.displayTracks(postList);
            //Create an AnimatedListViewAdapter that will manage the data and animations
            AnimatedListViewAdapter postListAdapter = new AnimatedListViewAdapter(this, viewResourceId, postList, objectMapper);
            //Tell the AnimatedListView to use the adapter
            animatedListView.setAdapter(postListAdapter);
        }
    }

    // Called to populate a View with the data of 'object'
    protected AnimatedListViewObjectMapper objectMapper = new AnimatedListViewObjectMapper() {
        @Override
        public void bindObjectToView(Object object, View view) {
            final AmbientTrack track = (AmbientTrack) object;
            //Populate and stylize the view however you want...

            ImageView image = (ImageView)view.findViewById(R.id.person_photo);

            new DownloadImageTask(image).execute(track.getAlbumImageUri().toString());

            TextView text = (TextView)view.findViewById(R.id.itemTextView);
            text.setText(track.getArtistName());

            TextView songName = (TextView)view.findViewById(R.id.songName);
            songName.setText(track.getName());

            ImageView share= (ImageView)view.findViewById(R.id.shareBtn);

            share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    shareIntent(track);
                }
            });
        }
    };
}


