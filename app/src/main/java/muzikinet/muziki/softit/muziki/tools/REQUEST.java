package muzikinet.muziki.softit.muziki.tools;

/**
 * Created by manyanda on 03/06/15.
 */
public enum REQUEST {
    NEW,
    SEARCH,
    POPULAR,
    PLAYLIST,
    HEARTHIS,
    SAVED
}
