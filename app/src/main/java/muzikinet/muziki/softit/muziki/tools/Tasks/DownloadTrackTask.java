package muzikinet.muziki.softit.muziki.tools.Tasks;

import android.app.DownloadManager;
import android.content.Context;
import muzikinet.muziki.softit.muziki.data.SongDBHelper;
import android.net.Uri;
import android.os.AsyncTask;
import android.widget.Toast;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import tonyostudios.ambience.AmbientTrack;

/**
 * Created by manyanda on 15/08/15.
 */
public class DownloadTrackTask extends AsyncTask<AmbientTrack,Void,AmbientTrack> {

    private Context context;

    public DownloadTrackTask(Context context) {
        this.context = context;
    }

    @Override
    protected AmbientTrack doInBackground(AmbientTrack... params) {

        String url = params[0].getAudioUri().toString();
        String[] urlContents = url.split("/");
        String FILENAME = urlContents[urlContents.length -1];

        FileOutputStream fos;
        try {
            InputStream in = new java.net.URL(url).openStream();
            if (!canSave()) {
                Toast.makeText(context, "No enough space available.", Toast.LENGTH_SHORT);
                return null;
            }

            byte data[] = new byte[1024];
            fos = context.openFileOutput(FILENAME, Context.MODE_PRIVATE);
            int count =0;
            while ((count = in.read(data)) != -1) {
                fos.write(data, 0, count);
            }

            fos.close();
        } catch (FileNotFoundException e) {
        } catch (IOException e) {
        }

        AmbientTrack track = AmbientTrack.newInstance();

        track.setId(params[0].getId());
        track.setAudioUri(Uri.parse(FILENAME));
        track.setArtistName(params[0].getArtistName());
        track.setName(params[0].getName());

        return track;
    }

    @Override
    protected void onPostExecute(AmbientTrack track) {
        if( track == null) {
            return;
        }

        SongDBHelper db = new SongDBHelper(context);
        db.addSong(track);
    }

    //TODO
    public void file_download(AmbientTrack track) {

        String url = track.getAudioUri().toString();
        String[] urlContents = url.split("/");
        /**
         * Filename musics/song_xxxxxxxx
         */
        String FILENAME = urlContents[urlContents.length-2] + "/" + urlContents[urlContents.length -1];

        DownloadManager mgr = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);

        DownloadManager.Request request = new DownloadManager.Request(track.getAudioUri());

        request.setAllowedNetworkTypes(
                DownloadManager.Request.NETWORK_WIFI
                        | DownloadManager.Request.NETWORK_MOBILE)
                .setAllowedOverRoaming(false).setTitle("Muziki.net mobile app")
                .setDescription("Downloading "+ track.getName() + " by "+ track.getArtistName())
                .setVisibleInDownloadsUi(true)
                .setDestinationInExternalPublicDir(context.getFilesDir().toString(), FILENAME);

        mgr.enqueue(request);

    }

    private boolean canSave() {
        /**
         * Make sure that at least 10 MB of space is available
         */
        return context.getFilesDir().getFreeSpace() > 10*1024*2024;
    }
}
