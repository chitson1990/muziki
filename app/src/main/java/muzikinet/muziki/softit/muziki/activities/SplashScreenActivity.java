package muzikinet.muziki.softit.muziki.activities;

import android.app.Activity;
import android.content.Intent;
import android.muziki.softit.muziki.R;
import android.os.Bundle;
import android.os.Handler;


public class SplashScreenActivity extends Activity {
    private static final int SPLASH_TIME_OUT = 500;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                Intent intentMainActivity = new Intent(SplashScreenActivity.this, NavigationDrawerActivity.class);
                startActivity(intentMainActivity);
                finish();
            }
        }, SPLASH_TIME_OUT);
    }

}
