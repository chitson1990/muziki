/*package android.muziki.softit.muziki.activities;


/**
 * Created by manyanda on 04/06/15.
 */
/*
import android.app.SearchManager;
import android.content.Intent;
import R;
import SongManager;
import HardCodedValues;
import REQUEST;
import android.os.Bundle;
import android.widget.Toast;

import com.kylewbanks.animlv.AnimatedListView;
import com.kylewbanks.animlv.AnimatedListViewAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import tonyostudios.ambience.AmbientTrack;


public class SearchActivity extends ListActivity {
    private String query;
    private  Map<String, String> queryData =new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("SEARCH RESULTS");
        handleIntent(getIntent());
    }


    private  void fetchData() {
        manager = new SongManager(REQUEST.SEARCH,mActivity);
        manager.render(queryData);
    }


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        animatedListView  = (AnimatedListView)findViewById(R.id.my_list);
        animatedListViewOnItemClickListener();

        animatedListViewOnItemLongClickListener();

        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            query = intent.getStringExtra(SearchManager.QUERY);
            if(query==null || query.isEmpty()) {
                //TODO  ask for input
                return;
            }

            queryData.put(HardCodedValues.query,query);
            fetchData();
        }
    }

    //Display data in the AnimatedListView
    @Override
    public void displayTracks(ArrayList<AmbientTrack> postList) {
        if(postList.size() == 0) Toast.makeText(this, "No Song Matches for your search,\n" +
                "Check your spelling or try a new search!", Toast.LENGTH_SHORT).show();
        else {
            super.displayTracks(postList);
            //Create an AnimatedListViewAdapter that will manage the data and animations
            AnimatedListViewAdapter postListAdapter = new AnimatedListViewAdapter(this, viewResourceId, postList, objectMapper);
            //Tell the AnimatedListView to use the adapter
            animatedListView.setAdapter(postListAdapter);
        }
    }
}
*/
package muzikinet.muziki.softit.muziki.activities;


/**
 * Created by manyanda on 04/06/15.
 */

import android.app.AlertDialog;
import android.app.SearchManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.muziki.softit.muziki.R;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.kylewbanks.animlv.AnimatedListView;
import com.kylewbanks.animlv.AnimatedListViewAdapter;
import com.kylewbanks.animlv.AnimatedListViewObjectMapper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import muzikinet.muziki.softit.muziki.data.SongManager;
import muzikinet.muziki.softit.muziki.tools.HardCodedValues;
import muzikinet.muziki.softit.muziki.tools.REQUEST;
import muzikinet.muziki.softit.muziki.tools.Tasks.DownloadImageTask;
import muzikinet.muziki.softit.muziki.viewholder.MuzikiPopUpWindow;
import tonyostudios.ambience.Ambience;
import tonyostudios.ambience.AmbientTrack;


public class SearchActivity extends NavigationDrawerActivity {
    private int viewResourceId = R.layout.recycler_item;
    private  AnimatedListView animatedListView;
    private  SongManager manager;
    private String query;

    private  Map<String, String> queryData =new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppThemeRed);
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.fragment_main, frameLayout);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container1, new MusicFragment())
                    .commit();
        }

        setTitle("SEARCH RESULTS");

        mActivity = this;

        animatedListView  = (AnimatedListView)findViewById(R.id.my_list);
        animatedListViewOnItemClickListener();
        animatedListViewOnItemLongClickListener();
        handleIntent(getIntent());
    }

    private  void fetchData() {
        manager = new SongManager(REQUEST.SEARCH,this);
        manager.render(queryData);
    }

    private  void animatedListViewOnItemClickListener() {
        animatedListView.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Ambience.activeInstance().PlayFromPosition(position);
                    }
                }
        );
    }


    private  void animatedListViewOnItemLongClickListener() {
        animatedListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

            public boolean onItemLongClick(AdapterView<?> arg0, View view,
                                           int pos, long id) {
                MuzikiPopUpWindow.newPopInstance(mActivity, HardCodedValues.dropDownList, pos).showAsDropDown(view, -5, 0);
                return true;
            }
        });
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        animatedListView  = (AnimatedListView)findViewById(R.id.my_list);
        animatedListViewOnItemClickListener();

        animatedListViewOnItemLongClickListener();

        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            query = intent.getStringExtra(SearchManager.QUERY);

            if(query==null || query.isEmpty()) {
                //TODO  ask for input
                return;
            }

            queryData.put(HardCodedValues.query,query);
            fetchData();
        }
    }

    private void notifyEmptyResults() {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        //TODO
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("No results found.").setPositiveButton("Retry", dialogClickListener).show();
    }

    //Display data in the AnimatedListView
    public void displayTracks(ArrayList<AmbientTrack> postList) {
        if(postList.size() == 0) Toast.makeText(this, " \n \nNo Song Matches,\n" +
                "Check your spelling or try a new search! \n \n ", Toast.LENGTH_SHORT).show();
        else {
            super.displayTracks(postList);
            //Create an AnimatedListViewAdapter that will manage the data and animations
            AnimatedListViewAdapter postListAdapter = new AnimatedListViewAdapter(this, viewResourceId, postList, objectMapper);
            //Tell the AnimatedListView to use the adapter
            animatedListView.setAdapter(postListAdapter);
        }
    }

    // Called to populate a View with the data of 'object'
    protected AnimatedListViewObjectMapper objectMapper = new AnimatedListViewObjectMapper() {
        @Override
        public void bindObjectToView(Object object, View view) {
            final AmbientTrack track = (AmbientTrack) object;
            //Populate and stylize the view however you want...

            ImageView image = (ImageView)view.findViewById(R.id.person_photo);

            new DownloadImageTask(image).execute(track.getAlbumImageUri().toString());

            TextView text = (TextView)view.findViewById(R.id.itemTextView);
            text.setText(track.getArtistName());

            TextView songName = (TextView)view.findViewById(R.id.songName);
            songName.setText(track.getName());

            ImageView share= (ImageView)view.findViewById(R.id.shareBtn);

            share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    shareIntent(track);
                }
            });
        }
    };

}
