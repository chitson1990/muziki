package muzikinet.muziki.softit.muziki.viewholder;

import android.graphics.Color;
import muzikinet.muziki.softit.muziki.activities.NavigationDrawerActivity;
import muzikinet.muziki.softit.muziki.data.SongDBHelper;
import muzikinet.muziki.softit.muziki.tools.Tasks.DownloadTrackTask;

import android.os.AsyncTask;
import android.os.Build;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import tonyostudios.ambience.Ambience;

/**
 * Created by manyanda on 09/06/15.
 */
public class MuzikiPopUpWindow {

    private final NavigationDrawerActivity context;
    private String[] popUpContents;
    PopupWindow popupWindow;
    private int position;

    public MuzikiPopUpWindow(NavigationDrawerActivity context, String[] contents, int positionOfSongToPlay) {
        this.context = context;
        popUpContents= contents;
        position= positionOfSongToPlay;
        
        popupWindow = new PopupWindow(context);

        // the drop down list is a list view
        ListView listViewDogs = new ListView(context);

        // set our adapter and pass our pop up window contents
        listViewDogs.setAdapter(popAdapter(popUpContents));

        // set the item click listener
        listViewDogs.setOnItemClickListener(new DogsDropdownOnItemClickListener());

        // some other visual settings
        popupWindow.setFocusable(true);
        popupWindow.setWidth(250);
        popupWindow.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);

        // set the list view as pop up window content
        popupWindow.setContentView(listViewDogs);
    }

    private PopupWindow getPop() {
        return popupWindow;
    }

    public static PopupWindow newPopInstance(NavigationDrawerActivity context, String[] contents, int selectedTrackPosition) {
        // initialize a pop up window type
        MuzikiPopUpWindow newInsance= new MuzikiPopUpWindow(context,contents, selectedTrackPosition);
        return newInsance.getPop();
    }

    /*
     * adapter where the list values will be set
     */
    private ArrayAdapter<String> popAdapter(String dataArray[]) {

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, dataArray) {

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                // setting the ID and text for every items in the list
                String item = getItem(position);
                String[] itemArr = item.split("::");
                String text = itemArr[0];
                String id = itemArr[0];

                // visual settings for the list item
                TextView listItem = new TextView(context);

                listItem.setText(text);
                listItem.setTag(id);
                listItem.setTextSize(22);
                listItem.setPadding(10, 10, 10, 10);
                listItem.setTextColor(Color.WHITE);

                return listItem;
            }
        };

        return adapter;
    }

    public class DogsDropdownOnItemClickListener implements AdapterView.OnItemClickListener {

        String TAG = "android.muziki.softit.muziki.viewholder.DogsDropdownOnItemClickListener.java";

        @Override
        public void onItemClick(AdapterView<?> arg0, View v, int arg2, long arg3) {
            Animation fadeInAnimation = AnimationUtils.loadAnimation(v.getContext(), android.R.anim.fade_in);
            fadeInAnimation.setDuration(10);
            v.startAnimation(fadeInAnimation);

            // dismiss the pop up
            popupWindow.dismiss();

            // get the id
            String selectedItemTag = v.getTag().toString();
            treatOnitemClick(selectedItemTag);
        }

        public void treatOnitemClick(String selectedTag) {
            switch(selectedTag) {
                case "Play":
                    Ambience.activeInstance().PlayFromPosition(position);
                    break;
                case "Share":
                    context.shareIntent(position);
                    break;
                case "Save":
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
                        new DownloadTrackTask(context).executeOnExecutor(
                                AsyncTask.THREAD_POOL_EXECUTOR, context.getTrack(position));
                    else
                        new DownloadTrackTask(context).execute(context.getTrack(position));
                    break;
                case "Remove":
                     new SongDBHelper(context).confirmDeletionOf(context.getTrack(position));
                default:
                    break;
            }
        }

    }
}
