package muzikinet.muziki.softit.muziki.viewholder;

import android.muziki.softit.muziki.R;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;


public  class ItemViewHolder extends RecyclerView.ViewHolder{

    private final TextView mItemTextView;

    public ItemViewHolder(final View parent, TextView itemTextView) {
        super(parent);
        mItemTextView = itemTextView;
    }

    public static ItemViewHolder newInstance(View parent) {
        TextView itemTextView = (TextView) parent.findViewById(R.id.itemTextView);
        return new ItemViewHolder(parent, itemTextView);

    }

    public void setItemText(CharSequence text) {
        mItemTextView.setText(text);
    }

   public TextView getItemTextView() {
        return mItemTextView;
    }
}
