package muzikinet.muziki.softit.muziki.adapters;

import android.content.Context;
import android.muziki.softit.muziki.R;
import android.os.AsyncTask;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.askerov.dynamicgrid.BaseDynamicGridAdapter;

import java.util.List;

import muzikinet.muziki.softit.muziki.tools.Tasks.DownloadImageTask;
import tonyostudios.ambience.AmbientTrack;

/**
 * Author:manyanda
 * Date: 9/6/15
 */
public class GridItemDynamicAdapter extends BaseDynamicGridAdapter {
    public GridItemDynamicAdapter(Context context, List<?> items, int columnCount) {
        super(context, items, columnCount);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        GridItemViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_grid, null);
            holder = new GridItemViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (GridItemViewHolder) convertView.getTag();
        }
        AmbientTrack track = (AmbientTrack) getItem(position);
        holder.build(track);
        return convertView;
    }

    private class GridItemViewHolder {
        private TextView artistName;
        private TextView songName;
        private ImageView image;

        private GridItemViewHolder(View view) {
            artistName = (TextView) view.findViewById(R.id.item_title);
            image = (ImageView) view.findViewById(R.id.item_img);
            songName = (TextView) view.findViewById(R.id.songName);
        }

        void build(AmbientTrack track) {
            artistName.setText(track.getArtistName());
            songName.setText(track.getName());
            /* executeOnExecutor() executes task in a different thread in case there's another AsyncTask
          running =>valid in post honeycomb, before honeycomb it runs sequentially as execute() ie. waits for
           the executing task to finish.*/
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
                new DownloadImageTask(image).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, track.getAlbumImageUri().toString());
            else
                new DownloadImageTask(image).execute(track.getAlbumImageUri().toString());

        }
    }
}
