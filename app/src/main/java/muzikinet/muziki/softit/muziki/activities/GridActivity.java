package muzikinet.muziki.softit.muziki.activities;

import android.muziki.softit.muziki.R;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;

import org.askerov.dynamicgrid.DynamicGridView;

import java.util.ArrayList;
import java.util.HashMap;

import muzikinet.muziki.softit.muziki.adapters.GridItemDynamicAdapter;
import muzikinet.muziki.softit.muziki.data.SongManager;
import muzikinet.muziki.softit.muziki.tools.HardCodedValues;
import muzikinet.muziki.softit.muziki.viewholder.MuzikiPopUpWindow;
import tonyostudios.ambience.AmbientTrack;


/**
 * Author:manyanda
 * Date: 9/6/15
 */

public class GridActivity extends NavigationDrawerActivity {
    private DynamicGridView gridView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppThemeRed);
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.activity_grid, frameLayout);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new MusicFragment())
                    .commit();
        }

        gridView = (DynamicGridView) findViewById(R.id.dynamic_grid);
        listView.setItemChecked(position, true);
        setTitle(navDrawerItems[position]);

        manager = new SongManager(request,this);
        manager.render(new HashMap<String, String>());

        mActivity= this;

//        add callback to stop edit mode if needed
//        gridView.setOnDropListener(new DynamicGridView.OnDropListener()
//        {
//            @Override
//            public void onActionDrop()
//            {
//                gridView.stopEditMode();
//            }
//        });

        // TODO shuffling
        gridView.setOnDragListener(new DynamicGridView.OnDragListener() {
            @Override
            public void onDragStarted(int position) {
            }                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   

            @Override
            public void onDragPositionsChanged(int oldPosition, int newPosition) {
            }
        });
        gridView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                gridView.startEditMode(position);
                return true;
            }
        });

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                MuzikiPopUpWindow.newPopInstance(GridActivity.this, HardCodedValues.dropDownList, position).showAsDropDown(view, -5, 0);
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (gridView.isEditMode()) {
            gridView.stopEditMode();
        } else {
            super.onBackPressed();
        }
    }

    public void displayTracks(ArrayList<AmbientTrack> tracks) {
        super.displayTracks(tracks);
        gridView.setAdapter(new GridItemDynamicAdapter(this,tracks,
                getResources().getInteger(R.integer.column_count)));
    }
}
